<?php
/**
 * StrawPoll
 * StrawPoll Hooks
 *
 * @package		StrawPoll
 *
 **/

class StrawPollHooks {
	/**
	 * Sets up this extension's parser functions.
	 *
	 * @access	public
	 * @param	object	Parser object passed as a reference.
	 * @return	boolean	true
	 */
	static public function onParserFirstCallInit( Parser &$parser ) {

		$parser->setHook( 'strawpoll', "StrawPollHooks::renderPoll");
	}

	/**
	 * Render strawpoll tag into iframe to strawpoll.
	 *
	 * @param string $input
	 * @param array $args
	 * @param Parser $parser
	 * @param PPFrame $frame
	 * @return string
	 */
	public static function renderPoll($input, array $args, Parser $parser, PPFrame $frame) {
		// we expext the input to be a strawpoll url
		$url = parse_url($input);
		if ($url === false) {
			return "<!-- couldn't parse input as url -->".$input;
		}
		if ((isset($url['host']) && ($url['host'] === "www.strawpoll.me" || $url['host'] === "strawpoll.me") ) || !isset($url['host'])) {
			$path = explode('/',$url['path']);
			$filtered = array_values(array_filter($path));
			$id = $filtered[0];
			if (!is_numeric($id)) {
				return "<!-- id was not numeric-->".$input;
			} else {
				$code = '<iframe src="https://www.strawpoll.me/embed_1/'.$id.'" style="border:0;"';
				if (!isset($args['width'])) {
					$args['width'] = "680";
				}
				if (!isset($args['height'])) {
					$args['height'] = "289";
				}
				foreach ($args as $name => $value) {
					if (is_string($value)) {
						$code .= " {$name}=\"{$value}\"";
					}
				}
				$code .= '><a href="https://www.strawpoll.me/'.$id.'" target="_blank">https://www.strawpoll.me/'.$id.'</a></iframe>';
				return $code;
			}
		}
		return $input;
	}
}
